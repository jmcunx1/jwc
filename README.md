## jwc -- Word Count and Line Length

jwc(1) is a clone of wc(1) but it includes an option for
minimum/maximum Line Length.  This was created for Systems
that does not have wc(1) and because I needed line lengths.

If wc(1) is present, you should use that since it is
much faster.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jwc) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jwc.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jwc.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
